﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour
{
    public Scrollbar ScrollRef;
    public RectTransform TargetObject;
    public Vector2 ClampValues;


    public void OnScroll()
    {
        float pos = Mathf.Lerp(ClampValues.x, ClampValues.y, ScrollRef.value);
        TargetObject.anchoredPosition = new Vector2(TargetObject.anchoredPosition.x, pos);
    }
}
