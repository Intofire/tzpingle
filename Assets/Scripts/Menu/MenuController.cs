﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public RectTransform Popup;
    public Animation PopupAnim;
    public Animation MenuHideAnim;
    public Transform Camera;

    public void StartGame()
    {
        if (Popup.anchoredPosition.x < 100)
            PopupAnim.Play("PopupHide");
        MenuHideAnim.Play("HideMenuButtons");
        StartCoroutine(DelayStart());
    }

    IEnumerator DelayStart()
    {
        for (float t = 0; t < 0.5f; t += Time.deltaTime)
        {
            yield return null;
            Camera.Translate(Vector3.down * Time.deltaTime * 8f);
        }
        SceneManager.LoadSceneAsync(1);
    }

    public void TogglePopup()
    {
        if (Popup.anchoredPosition.x < 100)        
            PopupAnim.Play("PopupHide");

        if (Popup.anchoredPosition.x > 1100)        
            PopupAnim.Play("PopupShow");
           
}

    

    public void Exit()
    {

        Application.Quit();
    }
}
