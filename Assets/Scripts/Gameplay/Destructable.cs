﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Destructable : MonoBehaviour
{
    public int HP = 1;
    public int MaxHP = 1;
    public intRef HPHolder;

    private void OnEnable()
    {
        if (HPHolder)
            HPHolder.Set(HP);
    }

    public void TakeDamage(int amount = 1)
    {
        HP-= amount;
        if (HPHolder)
            HPHolder.Set(HP);
        gameObject.SendMessage("OnHit", null, SendMessageOptions.DontRequireReceiver);

        if (HP <= 0)
        {
            Destruct();
            gameObject.SendMessage("OnDestruct", null, SendMessageOptions.DontRequireReceiver);
        }     
    }

    public virtual void Destruct()
    {
        foreach (MonoBehaviour mb in GetComponents<MonoBehaviour>())
            StopAllCoroutines();
        if (GetComponent<Collider>() != null)
            GetComponent<Collider>().enabled = false;
        StartCoroutine(DelayDestroy());
    }

    IEnumerator DelayDestroy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }
}
