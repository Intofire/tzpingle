﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOverTime : MonoBehaviour {

	public float Lifetime = 1.0f;
	
	void OnEnable () {
		StartCoroutine (Life());
	}	
	
	IEnumerator Life(){
		yield return new WaitForSeconds (Lifetime);
		Destroy(this.gameObject);		
	}
}
