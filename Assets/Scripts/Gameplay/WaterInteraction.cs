﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaterInteraction : MonoBehaviour
{
    public GameObject InteractionPrefab;

    private void OnTriggerEnter(Collider other)
    {
        Instantiate(InteractionPrefab, other.transform.position, Quaternion.identity);
        if (other.GetComponent<Character>() != null)
            other.GetComponent<Character>().enabled = false;
        if (other.CompareTag("Player"))
            StartCoroutine(DelayExit());
    }

    IEnumerator DelayExit()
    {
       yield return new WaitForSeconds(1f);           
       SceneManager.LoadSceneAsync(0);
    }
}
