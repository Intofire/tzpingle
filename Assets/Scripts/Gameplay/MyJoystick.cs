﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MyJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	public Vector2 inputVector = Vector2.zero;

	public RectTransform background;
	public RectTransform handle;

	public floatRef Horizontal;
	public floatRef Vertical;

	public float Disp = 1.0f;
	Vector2 joystickCenter = Vector2.zero;

	[Range(0f, 2f)] public float handleLimit = 1f;

	void FillRefs(){
		Horizontal.val = inputVector.x;
		Vertical.val = inputVector.y;
	}

	public virtual void OnDrag(PointerEventData eventData)
	{
		handle.position = eventData.position;
		Vector3 direction = handle.position - background.position;

		inputVector = direction.normalized;

		if ((direction / background.sizeDelta.x * 2.0f).magnitude > 1.2f)				 
			background.position =Vector3.Lerp(background.position, handle.position - direction * Disp, Time.deltaTime * 8.0f);		

		FillRefs ();
	}

	void OnEnable(){
		background.gameObject.SetActive(true);	
		handle.gameObject.SetActive(true);
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		inputVector = eventData.position - joystickCenter;
		background.gameObject.SetActive(true);
		handle.gameObject.SetActive(true);
		background.position = eventData.position;
		handle.anchoredPosition = background.anchoredPosition;

	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		background.gameObject.SetActive(false);	
		handle.gameObject.SetActive(false);
		inputVector = Vector2.zero;
		FillRefs ();
	}
}
