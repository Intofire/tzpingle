﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class intRef : ScriptableObject {

	public int Val = 0;      

    public static implicit operator int(intRef reference)
	{
		return reference.Val;

	}
	
    public void Set(int numb){
		Val = numb;
	}

    public void Increase(int amount = 1)
    {
        Val += amount;
    }

}
