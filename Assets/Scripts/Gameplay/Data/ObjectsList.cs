﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ObjectsList : ScriptableObject {

	public List<GameObject> HolderList;
	// Use this for initialization
	public void AddObject (GameObject go) {
		HolderList.Add (go);
	}
	
	// Update is called once per frame
	public void RemoveObject (GameObject go) {
		HolderList.Remove (go);
	}
}
