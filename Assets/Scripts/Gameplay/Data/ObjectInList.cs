﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInList : MonoBehaviour {
	public ObjectsList HolderList;
	// Use this for initialization
	void OnEnable () {
		HolderList.AddObject (this.gameObject);
	}
	
	// Update is called once per frame
	void OnDisable () {
		HolderList.RemoveObject (this.gameObject);
	}
}
