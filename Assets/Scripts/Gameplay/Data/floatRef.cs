﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class floatRef : ScriptableObject {
	public float val = 0;

	public static implicit operator float(floatRef reference)
	{
		return reference.val;
	}
}
