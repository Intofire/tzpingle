﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class intRefLabel : MonoBehaviour
{
    public intRef TargetValue;
    public TMP_Text TextLabel;

    void Update()
    {
        TextLabel.text = TargetValue.Val.ToString();
    }
}
