﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCameraControl : MonoBehaviour {

	public Vector3 CameraOffset;
	public Transform Target;
    public float Speed = 1f;

    void FixedUpdate () {
		if (Target != null) {
            transform.position = Vector3.Lerp(transform.position, Target.position + CameraOffset, Time.deltaTime * Speed);
		}
	}
}
