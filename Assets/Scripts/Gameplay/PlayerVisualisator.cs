﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Character))]
public class PlayerVisualisator : MonoBehaviour
{
    public GameObject OnHitPrefab;
    public ParticleSystem OnHitFX;
    public Transform LightSource;

    Character CharRef;
    Animator Anim;
    float lightHeight;

    void Start()
    {
        CharRef = GetComponent<Character>();
        Anim = GetComponent<Animator>();
        lightHeight = LightSource.position.y;
    }

    void Update()
    {
        float ang = Vector3.Angle(CharRef.moveDir, CharRef.lookDir) / 180f;
        ang = (ang - 0.5f) * (-2f);
        Anim.SetFloat("moveY", ang * CharRef.moveDir.magnitude);

        float right = Vector3.Angle(CharRef.moveDir, transform.right) / 180f;
        float left = Vector3.Angle(CharRef.moveDir, -transform.right) / 180f;
        right = right > left ? right : (-left);
        Anim.SetFloat("moveX", right * CharRef.moveDir.magnitude);
        LightSource.position = new Vector3(LightSource.position.x, lightHeight, LightSource.position.z);
        if (transform.position.y < -0.2f)
            Anim.SetBool("isFalling", true);
    }

    public void OnToolUse()
    {
        Anim.SetTrigger("attack"+Random.Range(1,3));       
    }

    public void OnHit()
    {
        Anim.SetTrigger("hit");
        OnHitFX.Play();
      GameObject hitFX =  Instantiate(OnHitPrefab, transform);
        hitFX.transform.localPosition = Vector3.zero;
    }
}
