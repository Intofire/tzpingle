﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : MonoBehaviour
{
    public GameObject SuicideFX;
    public ParticleSystem Trail;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player"))        
            StartCoroutine(SuicideAnim(other.gameObject));
        
    }

    IEnumerator SuicideAnim(GameObject player)
    {
        Trail.Stop();
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Animator>().SetTrigger("attack");
     
        yield return new WaitForSeconds(0.5f);
        Instantiate(SuicideFX, transform.position + Vector3.up, transform.rotation);
        player.GetComponent<Destructable>().TakeDamage();
        for (float t = 0; t < 1f; t += Time.deltaTime*4f)
        {
            yield return null;
            transform.localScale = Vector3.one * (1f - t);
        }
        Destroy(this.gameObject);
    }
    
}
