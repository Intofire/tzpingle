﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GetDirection : ScriptableObject {

	public virtual Vector3 GetDir (Transform actor) {
		return Vector3.zero;
	}

	public virtual bool IsActive () {
		return true;
	}
}
