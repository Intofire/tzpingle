﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "NewDir", menuName = "Direction/FollowTargetAI")]
public class FollowTargetDirectionAI : GetDirection
{
    public ObjectsList Targets;

    public override Vector3 GetDir(Transform actor)
    {
        float shortestDistance = 999.0f;
        int target = 999;
        float temp = 0;
        for (int i = 0; i < Targets.HolderList.Count; i++)
        {
            temp = Vector3.Distance(Targets.HolderList[i].transform.position, actor.position);
            if (temp < shortestDistance)
            {
                target = i;
                shortestDistance = temp;
            }
        }
        if (target == 999)
            return Vector3.zero; 

        NavMeshAgent Agent = actor.GetComponentInChildren<NavMeshAgent>();
        Agent.SetDestination(Targets.HolderList[target].transform.position);

        if (Agent.path.status == NavMeshPathStatus.PathComplete && Agent.path.corners.Length > 1)
        {
            Vector3 lookPos = Agent.path.corners[1] - Agent.transform.position;
            lookPos.y = 0;           
            return lookPos;
        }

        return Vector3.zero;
    }
}
