﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDir", menuName = "Direction/Forward")]
public class ForwardDirection : GetDirection
{
    public override Vector3 GetDir(Transform actor)
    {        
        return actor.forward;
    }
}
