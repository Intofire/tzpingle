﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NewDir", menuName = "Direction/InputMouse")]
public class InputMouseDirection : GetDirection {
	Plane plane;	
	Ray ray;
	Vector3 hitPoint;

	public override Vector3 GetDir (Transform actor) {
		plane = new Plane(Vector3.up, actor.position);
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		float distance = 20.0f;
		if (plane.Raycast(ray, out distance)){
			hitPoint = ray.GetPoint(distance);		
		} 

		return hitPoint- actor.position;
	}
}
