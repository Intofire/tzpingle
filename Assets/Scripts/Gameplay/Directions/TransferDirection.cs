﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDir", menuName = "Direction/Transfer")]
public class TransferDirection : GetDirection {
	Vector3 forward;
	Vector3 right;
	public floatRef HorizontalAxis;
	public floatRef VerticalAxis;

	public override Vector3 GetDir (Transform actor) {
		forward = Camera.main.transform.TransformDirection(Vector3.forward);
		forward.y = 0;
		forward = forward.normalized;
		right  = new Vector3(forward.z, 0, -forward.x);
		return HorizontalAxis * right + VerticalAxis * forward;

	}

}