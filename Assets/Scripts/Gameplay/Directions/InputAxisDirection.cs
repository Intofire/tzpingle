﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NewDir", menuName = "Direction/InputAxis")]
public class InputAxisDirection : GetDirection {
	Vector3 forward;
	Vector3 right;
	public string HorizontalAxis = "Horizontal";
	public string VerticalAxis = "Vertical";

	public override Vector3 GetDir (Transform actor) {
		forward = Camera.main.transform.TransformDirection(Vector3.forward);
		forward.y = 0;
		forward = forward.normalized;
		right  = new Vector3(forward.z, 0, -forward.x);
		return Input.GetAxis(HorizontalAxis) * right + Input.GetAxis(VerticalAxis) * forward;

	}

	public override bool IsActive () {
        return (Input.GetAxisRaw(HorizontalAxis) != 0 || Input.GetAxisRaw(VerticalAxis) != 0);			
	}
}
