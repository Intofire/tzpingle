﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDir", menuName = "Direction/TargetVelocity")]
public class TargetVelocityDirection : GetDirection {
	public ObjectsList Targets;
	public float Multiplier = 4.0f;
	Transform target;
	public override Vector3 GetDir (Transform actor) {
		

		if (Targets.HolderList.Count != 1)
			return Vector3.zero;
		else {
			target = Targets.HolderList [0].transform;
			float vel = target.GetComponent<Rigidbody> ().velocity.magnitude;
			if (vel>30)
				return target.position  +  target.forward*Multiplier*vel - actor.position; // target.forward * vel.magnitude * Multiplier
			else
				return target.position  - actor.position; 
		}

	}
}
