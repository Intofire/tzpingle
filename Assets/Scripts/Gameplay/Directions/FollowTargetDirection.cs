﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDir", menuName = "Direction/FollowTarget")]
public class FollowTargetDirection : GetDirection {
	public ObjectsList Targets;
	public override Vector3 GetDir (Transform actor) {
		float shortestDistance = 999.0f;
		int target = 999;
		float temp = 0;
		for (int i = 0; i < Targets.HolderList.Count; i++) {
			temp = Vector3.Distance (Targets.HolderList [i].transform.position, actor.position);
			if (temp < shortestDistance) {
				target = i;
				shortestDistance = temp;
			}
		}
		if (target == 999)
			return Vector3.zero;
		else
			return Targets.HolderList [target].transform.position- actor.position;

	}
}
