﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PickUp : MonoBehaviour {
    public UnityEvent OnPickUp;
    public GameObject PickUpVFX;
   
	void OnEnable(){
		StopAllCoroutines ();
        transform.eulerAngles = Vector3.up * Random.Range(0, 360f);
		GetComponent<Collider> ().enabled = true;
	}

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player"))
			StartCoroutine (PickupAnim(other.transform));		
	}

	IEnumerator PickupAnim(Transform target){			
		GetComponent<Collider> ().enabled = false;
        OnPickUp.Invoke();
        Instantiate(PickUpVFX, transform.position, Quaternion.identity);
        Vector3 startPos = transform.position;
		for (float step = 0; step < 1.0f; step += Time.deltaTime*4f) {
			if (transform == null || target == null)
				break;
			transform.position = Vector3.Lerp (startPos, target.position + Vector3.up, step);
            transform.localScale = Vector3.one * (1f - step);
            yield return null;
		}		
		Destroy (this.gameObject);
	}

	

}
