﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{   
    public GetDirection MoveDirection;
    public GetDirection LookDirection;
    public float Speed = 1.0f;
   
    [HideInInspector]
    public Vector3 lookDir;
    [HideInInspector]
    public Vector3 moveDir;

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        lookDir = LookDirection.GetDir(transform);
        float lookAngle = 180 / Mathf.PI * Mathf.Atan2(lookDir.x, lookDir.z);
        if (lookDir.sqrMagnitude > 0)
            rb.MoveRotation(Quaternion.Lerp(transform.rotation, Quaternion.Euler(Vector3.up * lookAngle), Time.deltaTime * Speed * 2f));
        if (transform.position.y > -0.1f)
            moveDir = MoveDirection.GetDir(transform);
        if (moveDir.sqrMagnitude > 1.0f)
            moveDir.Normalize();
        if (transform.position.y > -0.1f)
            rb.MovePosition(transform.position + moveDir * Speed * Time.deltaTime);
    }
}
