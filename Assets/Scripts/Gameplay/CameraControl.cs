﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
	public ObjectsList Targets;
	public Vector3 CameraOffset;
	public Transform Target;
	public floatRef SizeHor;
	public floatRef SizeVer;


	public Camera Cam;
	public float MinFov = 50.0f;
	public float MaxFov = 70.0f;
	public float FovCoeff = 0.5f;

	public float ShakeAmplitude = 0.5f;
	public float ShakeDuration = 0.5f;

	public float Offset;
	public float OffsetVert;
	// Use this for initialization
	[Range(0, 1)]
	public float OfsetMultiplier;

	void OnEnable(){
		Cam = GetComponent<Camera> ();		
	}	

	void OnHit(Transform targer){
		if (targer.CompareTag("Player"))
			StartCoroutine (Shake ());			
	}

	IEnumerator Shake(){
		for (float step = 0; step < ShakeDuration && Target!= null; step+=Time.deltaTime) {
			float power = ShakeAmplitude * (ShakeDuration - step);
			Vector3 rand = new Vector3 (
				Random.Range(-power, power), 
				0, 
				Random.Range(-power, power));
			transform.position = Target.position + CameraOffset*OfsetMultiplier + rand;
			ClampCam ();
			yield return null;
		}
	}

	void ClampCam(){
		transform.position = new Vector3 (
			Mathf.Clamp (transform.position.x, -SizeHor * 0.5f + Offset, SizeHor * 0.5f  - Offset), 
			transform.position.y, 
			Mathf.Clamp (transform.position.z, -SizeVer * 0.5f - Offset, SizeVer * 0.5f - OffsetVert));
	}

	void Update () {
		if (Target != null) {
			transform.position = Target.position + CameraOffset * OfsetMultiplier;
	
			Cam.fieldOfView = Mathf.Lerp (
				Cam.fieldOfView, 
				Mathf.Clamp (Target.GetComponent<Rigidbody> ().velocity.magnitude * FovCoeff, MinFov, MaxFov), 
				Time.deltaTime);
		
			ClampCam ();
		}
	}
}
